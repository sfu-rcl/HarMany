# HarMany
## Overview
The HarMany library allows processors to control hardware accelerators without the use of an operating system.

Provided as a standalone C library, it is designed to support:
- Safe and synchronized multithreaded access to hardware accelerators
- Passing an arbitrary quantity of arguments to hardware accelerators
- Calling a specified interrupt handler when an accelerator has finished a job
- Nonblocking accelerator invocations thanks to hardware argument FIFOs

The HarMany library was designed for RISC-V with the [CVA5](https://github.com/openhwgroup/cva5) processor in mind.

## Documentation
This project uses [Doxygen](https://www.doxygen.nl/) for software documentation. Simply run the `doxygen` command to view the HTML report.
I've also provided some example software and hardware files as a starting point.

## Compilation
### Software
The HarMany library can be configured using the HarManyCfg.h file.
The library itself does not require any special compilation directives. Just note that HarMany.c depends on HarMany.h, and both of those files depend on HarManyCfg.h.

Note that special care needs to be taken when linking to reserve the special memory locations specified in HarManyCfg.h.

Minor changes to internal functionality in HarMany.c might need to be done if the CVA5 processor is not being used. The inline assembly instructions are a good place to check first.

### Hardware
Each type of hardware accelerator in the system requires its own custom hardware wrapper. You may wish to reuse the hardware components that I've provided.
The environment in the examples folder contains examples of two different wrappers.

The hardware wrapper components should not be difficult to integrate into your system, as they require only three ports:
1. Memory I/O port
2. Processor interrupt ID
3. Processor interrupt valid

## Licensing
The HarMany library itself (hw, sw, doc folders) is licensed under the Apache V2.0 license.

However, in addtion to new files (also licensed under Apache V2.0), the examples folder also contains components from different sources with different licenses:
- board_support.c, board_support.h: taken from the [taiga example project](https://gitlab.com/sfu-rcl/taiga-example-c-project)
- chacha.c, chacha.h: taken from [Oryx Embedded](https://www.oryx-embedded.com), GPL V2 or later license
- sha256.c, sha256.h: taken from [this repo](https://github.com/B-Con/crypto-algorithms)
- chacha_core.v, chacha_qr.v: taken from [this repo](https://github.com/secworks/chacha), BSD 2-clause license
- sha256_core.v, sha256_k_constants.v: taken from [this repo](https://github.com/secworks/sha256), BSD 2-clause license
- CVA5: taken from the [OpenHW Group](https://github.com/openhwgroup/cva5), Apache V2.0 license

## Example
The examples folder contains a demonstration of HarMany with two processors and two hardware accelerators. Refer to that directory for more details.