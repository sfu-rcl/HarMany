Hardware Interface               {#hw}
===

\section over_sec Overview
The HarMany software-hardware interface is done through two components: the **mailbox**, and the **arguments**.
These components are fixed memory locations where the communication between hardware accelerators and the HarMany software library occurs.

The location of these components is specified in software in @ref HarManyCfg.h, and as constants in the @ref dmem.

\section mail_sec Mailbox
The mailbox is a fixed 32-bit memory address that controls the hardware accelerator. Naturally, its location must be 32-bit aligned. It has the following bit structure:
| 31-16  | 15-10        | 9-8              | 7-2        | 1-0   |
| :---:  | :---:        | :---:            | :---:      | :---: |
| Custom | Response CPU | Response Pending | Target CPU | State |


\subsection cus_sec Custom bits (31-16)
The upper 16 bits in the mailbox are reserved for custom use by an implementation.
They can be [read](@ref read_custom) or [written](@ref write_custom) by software at any time.
They are also optionally written by software in a call to @ref call_accelerator, where they will be seen in the upper 16 bits of the first argument returned by the [argument manager](@ref imem).
Additionally, these bits are automatically passed as the second argument to an interrupt handler specified by @ref register_interrupt_handler or @ref change_interrupt_handler.

The meaning and access patterns of these bits are specified by the system designer. For example, they can be used as an extra 16-bit argument, a 16-bit return value, or accelerator status bits.

\subsection resp_sec Response bits (15-8)
These bits are used by accelerators when [signalling an interrupt](@ref dint_sec). The upper 6 bits indicate the processor to which the interrupt is being delivered (originally specified by the upper 6 bits in @ref state_sec), and the lower 2 bits are used to indicate validity (11 = valid, anything else = invalid).

An [accelerator wrapper](@ref dmem) must write the interrupt CPU and the valid bits once it has finished a job that requires an interrupt response. It must wait for them to be cleared before any further interrupts can be issued.

Implementation note: This does not mean that the hardware accelerator must block until a response is received after every interrupt. It means that the hardware accelerator must block if it wishes to issue another interrupt and there is an interrupt that is still pending.

In the case where one processor can potentially receive interrupts from multiple hardware accelerators, it is useful for an interrupt handler to know the particular cause of the interrupt. This information is currently passed as the first argument to the handler specified by @ref register_interrupt_handler or @ref change_interrupt_handler.
However, because all hardware accelerators are connected to the same external interrupt port in the processor, it is impossible for the processor to determine which accelerator caused an interrupt.
This is where these 8 bits become useful. When an interrupt is triggered, the processor first enters the internal @ref irq_handler function, which scans through all mailboxes until it finds the pending interrupt. Before the user-specified interrupt is handler, these response bits are cleared by the processor.

Implementation note: The format of the pending interrupt response bits matches the format of the [state](@ref state_sec) bits when the accelerator was invoked and an interrupt was requested (both have the form (CPU << 6) | 3). The latter are stored automatically as the lower 8 bits of the first argument in the [argument FIFO](@ref ififo_sec).

\subsection state_sec State bits (7-0)
These state bits are the most important bits in the mailbox, as they are responsible for submitting a task to to an accelerator.
The upper 6 bits specify the target CPU for an interrupt (only if specified), and the lower two bits specify the state.

The following state diagram depicts the interactions between @ref call_accelerator, the @ref dmem, and the @ref imem.

![](/src/doc/state.png)

It is important to recognize which components are responsible for state transitions. The READY state can only be left by a processor, and leads to entry into either PENDING or PENDING_INTERRUPT.
At this point the [interface manager](@ref imem) takes over, and places the arguments into the [interface FIFO](@ref ififo_sec). If the FIFO is full, it enters the FULL state and eventually enters the READY state. If not, it directly transitions to the READY state.

\section arg_sec Arguments
Each hardware accelerator has a fixed number of 32-bit arguments that must reside in a contiguous section of 32-bit aligned memory.
The quantity of arguments for an accelerator is specified in @ref HarManyCfg.h, and the @ref imem.

When @ref call_accelerator is called by software, the specified arguments are written to the argument memory address.
These arguments are then read by the [interface manager](@ref imem) and automatically placed into the [argument FIFO](@ref ififo_sec) for use in the [accelerator wrapper](@ref dmem).

The meaning and interpretation of these arguments is completely up to the designer.
The most important thing is that an argument has an identical meaning to the wrapper designer as well as the software programmer.
This can be facillitated by passing stateless arguments and fixing the ordering of the arguments.
Another thing worth considering is that the arguments are written and read in sequential 32-bit pieces.
This lends itself nicely to arguments of pointers and scalars, but poorly to more complex data structures.
For example, if a raw struct is casted to a uint32_t* and passed to @ref call_accelerator, careless reordering of members or insufficient consideration of padding may lead to desynchronization between software and hardware.

