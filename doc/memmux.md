Memory Multiplexer              {#memmux}
===

\section mem_sec Overview
Currently the [hardware accelerator wrapper](@ref dmem) is connected to a private L1 data cache.
The L1 cache has only a single port, but in a typical implementation at least two ports are needed.
One for the [data port](@ref dcom_sec), and one for the [instruction port](@ref imem).
This necessitates an arbiter to allow the two ports to share access.

\section mport_sec Ports
- *input* **logic** clk: Clock for the memory multiplexer.
- *input* **logic** rst: Synchronous active-high reset.
- **memory_sub_unit_interface.responder** low_prio: The lower priority memory port.
- **memory_sub_unit_interface.responder** high_prio: The higher priority memory port.
- **memory_sub_unit_interface.controller** l1: The real memory port.

\section prio_sec Priority
The high priority port overrules the low priority port, meaning that it will appear that *low_prio* is not ready if *high_prio* makes a memory request.
This can lead to starvation, so carefully consider their respective memory access patterns.

