Hardware Interface Manager              {#imem}
===

\section imem_sec Overview
I've provided a SystemVerilog module for managing the mailbox state and reading arguments.
This allows the [accelerator wrapper](@ref dmem) to focus solely on managing the hardware accelerator state and reading and writing the neccessary data.

\section iparam_sec Parameters
- **logic[31:0]** MAILBOX_ADDR: The fixed memory address of the mailbox
- **logic[31:0]** ARG_ADDR: The fixed address to the first hardware accelerator argument. All subsequent arguments must immediately follow.
- NUM_ARGS: The number of 32-bit arguments to be read.
- FIFO_DEPTH: The depth of the argument FIFO in terms of groups of arguments.

\section iport_sec Ports
- *input* **logic** clk: Clock for the interface manager.
- *input* **logic** rst: Synchronous active-high reset.
- *input* **logic** fifo_pop: Removes the first element in the FIFO.
- *output* **logic** fifo_valid: Indicates the validity of fifo_data.
- *output* **logic[31:0]** fifo_data: The current 32-bit argument in the FIFO.
- **memory_sub_unit_interface.controller** i_mem: The memory port allowing the wrapper to make memory requests (it does so continuously).

\section ififo_sec Argument FIFO
The interface FIFO contains arguments for queued requests to the hardware accelerator. It has a width of 32 bits, and a depth of (NUM_ARGS+1)*FIFO_DEPTH bits.
Arguments are automatically pushed by this file, and are popped by the [accelerator wrapper](@ref dmem).

The arguments passed to @ref call_accelerator are ordered and visible in this FIFO.
However, there is one extra argument that is prepended to a group of arguments.
This argument is a copy of the mailbox at the time the accelerator left the READY state.
This thus contains the [custom bits](@ref cus_sec), as well as information about the desired interrupt contained in the [state bits](@ref state_sec).

All arguments in a group must be popped as quickly as fifo_valid will allow. Otherwise, the FIFO may potentially overflow.

Implementation note: The hardware interface wrapper is implemented using a finite state machine.

