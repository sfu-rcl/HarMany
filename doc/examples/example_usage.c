#include <stdio.h>
#include "HarMany.h"

//In this demonstration there is a single hardware accelerator with the following behaviour:
//It writes its first argument to the address specified by its second argument, and places an error status in the mailbox

//Volatile is useful for variables a hardware accelerator can write to
static volatile uint32_t responses[8];

//Volatile is also useful for variables an interrupt can modify
static volatile uint8_t jobs;
static volatile bool error;
static const options_t irq_flags = {.interrupt = true, .return_if_busy = true, .return_if_full = true, .write_custom = false};

void irq(uint8_t id, uint16_t custom) {
    //Check the return values
    if (id || custom || responses[jobs] != jobs) {
        error = true;
        return;
    }
    if (++jobs < 8) {
        //Note the casts to uint32_t when passing the arguments to the library
        if (call_accelerator(0, (const uint32_t[]){(uint32_t) jobs, (uint32_t) &responses[jobs]}, 0, 0, irq_flags) < 0)
            error = true;
    }
}


int main(void) {

    result_t res;
    options_t flags;

    //This function will be called when the accelerator finishes with a job and an interrupt was requested
    register_interrupt_handler(irq);

    //Compound literals can be used to allocate all fields simultaneously
    flags = (options_t){.interrupt = true, .return_if_busy = false, .return_if_full = false, .write_custom = false};
    
    //Compound literals can also be used when creating the argument array to prevent clutter
    res = call_accelerator(0, (const uint32_t[]){0, (uint32_t) &responses}, 0, 0, flags);
    if (res != SUCCESS)
        return -1;
    
    //At this point the processor can do whatever jobs it wants, the interrupts ensure subsequent jobs are queued automatically

    //Waiting on variables modified in the interrupt can be used to synchronize regular code and hardware accelerators
    while (jobs != 9 || !error);
    if (error) 
        printf("Failure\n");
    else
        printf("Success\n");

    return error;
}
