#ifndef HARMANYCFG_H
#define HARMANYCFG_H

//Common options to all configurations
#define NUM_ACCELERATORS 3
#define MAILBOX_ADDRESSES {0x80001000, 0x80001010, 0x80002000}
#define ARG_BASE_ADDRESSES {0x80001004, 0x80001014, 0x80003000}
#define ARG_COUNTS {3, 7, 4}
//It is optional for arguments to be adjacent to their mailbox, but recommended for cache performance

//Runtime bounds checks are disabled
#define CHECK_INPUT 0

//Interrupts are enabled
#define INTERRUPT 1

#define NUM_CORES 2

//Default thread safety is enabled but no atomic instructions are used
#if NUM_CORES > 1 || INTERRUPT != 0
    #define THREAD_SAFE 1
    #if THREAD_SAFE != 0
        #define USE_ATOMIC 0
        #if NUM_CORES > 1
            #define MUTEX_BASE_ADDRESS 0x80004000
        #endif
    #endif
#endif

#endif
