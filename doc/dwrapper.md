Hardware Accelerator Wrapper              {#dmem}
===

\section dmem_sec Overview
The hardware accelerator wrapper is where the ports of the hardware accelerator itself are managed.
It gets arguments from the @ref imem, and is capable of reading any extra input data from main memory.
The hardware accelerator wrapper also writes out data from the accelerator to memory as needed, and may potentially issue an interrupt once a task has completed.

\section dport_sec Ports
- *input* **logic** clk: Clock for the hardware accelerator.
- *input* **logic** rst: Synchronous active-high reset.
- **memory_sub_unit_interface.controller** req: The memory port allowing the wrapper to make memory requests.
- *output* **logic** interrupt: Indicates the validity of interrupt_cpu
- *output* **logic[5:0]** interrupt_cpu: The processor number to interrupt.

\section dcom_sec Composition
The hardware accelerator wrapper is primarily written by a user of this library.
It contains the hardware accelerator itself, alongside the @ref imem and the @ref memmux.

The design and implementation of this wrapper can vary significantly according to the memory access patterns of the hardware accelerator.
For many designs, a simple finite state machine with the following generic states is likely suitable:
- IDLE: Do nothing or reset the accelerator.
- READ_ARG: Load arguments from the [FIFO](@ref ififo_sec) into local registers.
- READ_DATA: Read and buffer the input data into local registers or scratch memory.
- SUBMIT: Launch the hardware accelerator with the configuration parameters and the local data.
- WRITE: Write the output data.
- INTERRUPT: Wait for a response to a pending interrupt.

More sophisticated designs could pipeline these states and overlap them for different accelerator invocations.

\section dint_sec Interrupts
It is the responsibility of this wrapper to submit interrupts when they are requested.
Interrupts are requested according to the [lower 8 bits](@ref state_sec) of the first argument in the [interface manager FIFO](@ref ififo_sec).
When a job with an interrupt is finished, the wrapper must do two things:
1. Assert *interrupt* and *interrupt_cpu* until the interrupt has been cleared.
2. Write the target CPU and the valid bit to the [response bits](@ref resp_sec).

Pending interrupts may not overlap for a hardware accelerator. The previous interrupt must be acknowledged (by the @ref irq_handler when it clears the [interrupt response bits](@ref state_sec)) before the next interrupt can be delivered.

