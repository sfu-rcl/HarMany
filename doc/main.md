\mainpage HarMany Project

\section intro_sec Introduction

The HarMany project is a C-language software library for interfacing with hardware accelerators in an FPGA system.
It is designed specificially to be compatible with the [CVA5](https://github.com/openhwgroup/cva5) RISC-V soft processor.

\section feat_sec Features
1. Safe and synchronized multithreaded access to hardware accelerators
2. Passing arbitrary quantity of arguments to hardware accelerators
3. Calling a specified interrupt handler when an accelerator has finished a job
4. Nonblocking accelerator invocations thanks to hardware argument FIFOs

\section comp_sec Compilation Instructions
The HarMany libary does not require any special compilation flags or directives and should compile without issue on a standard 32-bit RISC-V compiler.
The @ref HarMany.c library depends on the @ref HarMany.h and @ref HarManyCfg.h files.
Code that calls functions in the library should link to HarMany.o and include @ref HarMany.h.

The HarMany library reserves some memory locations for special purposes according to @ref HarManyCfg.h.
These locations must not be used by anything else and must be initialized to 0, both of which can be done by the linker script.

\section exam_sec Examples
For software usage and documentation, see @ref example_usage.c and @ref HarMany.h respectively.

For hardware interface documentation and components, see @ref hw.

