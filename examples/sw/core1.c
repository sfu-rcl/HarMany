#include "board_support.h"
#include <stdio.h>
#include <string.h>
#include "HarMany.h"
#include "chacha.h"
#include "sha256.h"

#define CHA_ROUNDS 20
#define CHA_KEY_LEN 32
#define CHA_IV_LEN 8
#define CHA_MSG_LEN 128
static const uint8_t chaKey1[CHA_KEY_LEN];
static const uint8_t chaIv1[CHA_IV_LEN];
static const uint8_t chaMsg1[128];
static uint8_t chaCPU1[CHA_MSG_LEN];
static const uint8_t chaKey2[64] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
static const uint8_t chaIv2[CHA_IV_LEN] = {7, 6, 5, 4, 3, 2, 1, 0};
static const uint8_t chaMsg2[500] = "Now is the time for all good men to come to the aid of the party.";
static const uint8_t shaInput3[] = {"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"};

static uint8_t* sharedStart = (uint8_t*) 0x8000E000;

extern unsigned long long _read_cycle();

static volatile uint8_t flag;
void hd(uint8_t id, uint16_t custom) {
    if (id == 1 && custom == 5)
        flag = 1;
}

volatile unsigned* shared = (unsigned*) 0x8000FFF0;

int main(void) {
    //Platform Initialization
    platform_init ();

    //Records cycle and instruction counts
    start_profiling ();

    register_interrupt_handler(hd);

    uint64_t startCycle;
    uint64_t stopCycle;
    result_t res;
    options_t flags;
    //Args: Num rounds, IV LSB, IV MSB, Key*, KeyIs256, Src*, Dst*, Len
    uint32_t chaArgs[8];

    //Initialize variables while core 0 does its thing
    ChachaContext cha_ctx;
    chachaInit(&cha_ctx, CHA_ROUNDS, chaKey1, CHA_KEY_LEN, chaIv1, CHA_IV_LEN);

    chaArgs[0] = CHA_ROUNDS;
    chaArgs[1] = *(uint32_t*) chaIv1;
    chaArgs[2] = *(uint32_t*) &chaIv1[4],
    chaArgs[3] = (uint32_t) chaKey1;
    chaArgs[4] = CHA_KEY_LEN*8 == 256;
    chaArgs[5] = (uint32_t) chaMsg1;
    chaArgs[6] = (uint32_t) sharedStart;
    chaArgs[7] = CHA_MSG_LEN;
    flags = (options_t){.interrupt = true, .return_if_busy = false, .return_if_full = false, .write_custom = true};
    
    //Wait until core 0 is finished to send it a job
    while (*shared != 1);
    res = call_accelerator(1, chaArgs, 4, 0, flags);
    
    uint32_t shaArgs[3];
    flags = (options_t){.interrupt = false, .return_if_busy = false, .return_if_full = false, .write_custom = false};
    shaArgs[0] = sizeof(shaInput3)-1;
    shaArgs[1] = (uint32_t) shaInput3;

    //Wait until core 0 indicates we should start flooding the first accelerator with requests
    while (*shared != 3);
    for (int i = 1; i < 10; i += 2) {
        shaArgs[2] = (uint32_t) (sharedStart + i*SHA256_BLOCK_SIZE);
        res |= call_accelerator(0, shaArgs, 0, 0, flags);
    }

    //Wait until core 0 is done printing so we can start
    while (*shared != 4);
    printf("Control has been given to core 1.\n");
    if (res != SUCCESS) {
        printf("Calling accelerator returned %d\n", res);
        return -1;
    }

    printf("Timing software CHACHA...\n");
    startCycle = _read_cycle();
    chachaCipher(&cha_ctx, NULL, chaCPU1, CHA_MSG_LEN);
    stopCycle = _read_cycle();
    printf("Took %llu cycles\n", stopCycle-startCycle);

    printf("Timing hardware CHACHA...\n");
    flags = (options_t){.interrupt = true, .return_if_busy = false, .return_if_full = false, .write_custom = true};
    chaArgs[6] = (uint32_t) sharedStart+CHA_MSG_LEN;
    startCycle = _read_cycle();
    res = call_accelerator(1, chaArgs, 5, 1, flags);
    while (flag != 1 || res != SUCCESS);
    stopCycle = _read_cycle();
    printf("Took %llu cycles\n", stopCycle-startCycle);
    if (res != SUCCESS) {
        printf("Calling accelerator returned %d\n", res);
        return -1;
    }
    printf("The comparison between software and hardware methods is %d\n", memcmp(chaCPU1, (void*) chaArgs[6], CHA_MSG_LEN));

    printf("Submitting lengthy jobs to test blocking...\n");
    flags.interrupt = false;
    flags.return_if_busy = false;
    flags.return_if_full = false;
    flags.write_custom = false;
    chaArgs[0] = CHA_ROUNDS; //Slow it as much as possible
    chaArgs[1] = *(uint32_t*) chaIv2;
    chaArgs[2] = *(uint32_t*) &chaIv2[4],
    chaArgs[3] = (uint32_t) chaKey2;
    chaArgs[4] = false;
    chaArgs[5] = (uint32_t) chaMsg2;
    chaArgs[6] = (uint32_t) sharedStart;
    chaArgs[7] = sizeof(chaMsg2);
    //First big encryption job
    res = call_accelerator(1, chaArgs, 0, 1, flags);
    if (res != SUCCESS) {
        printf("First accelerator call returned %d\n", res);
        return -1;
    }
    chaArgs[6] = (uint32_t) (sharedStart + sizeof(chaMsg2));
    //Second big encryption job is accepted and stored in FIFO
    res = call_accelerator(1, chaArgs, 0, 1, flags);
    if (res != SUCCESS) {
        printf("Second accelerator call returned %d\n", res);
        return -1;
    }
    chaArgs[6] = (uint32_t) (sharedStart + 2*sizeof(chaMsg2));
    //Third big job is refused
    flags.return_if_busy = true;
    flags.return_if_full = true;
    res = call_accelerator(1, chaArgs, 0, 1, flags);
    if (res == SUCCESS) {
        printf("Job was incorrectly accepted\n");
        return -1;
    }
    chaArgs[5] = (uint32_t) sharedStart;
    chaArgs[6] = (uint32_t) sharedStart;
    flags.return_if_busy = false;
    flags.return_if_full = false;
    //Fourth blocking decryption job is accepted
    res = call_accelerator(1, chaArgs, 0, 1, flags);
    if (res != SUCCESS) {
        printf("Fourth accelerator call returned %d\n", res);
        return -1;
    }
    chaArgs[5] = (uint32_t) (sharedStart + sizeof(chaMsg2));
    chaArgs[6] = (uint32_t) (sharedStart + sizeof(chaMsg2));
    //Fifth blocking decryption job is accepted
    res = call_accelerator(1, chaArgs, 0, 1, flags);
    if (res != SUCCESS) {
        printf("Fifth accelerator call returned %d\n", res);
        return -1;
    }
    printf("The first encryption/decryption comparison is %d\n", memcmp(chaMsg2, sharedStart, sizeof(chaMsg2)));
    printf("The second encryption/decryption comparison is %d\n", memcmp(chaMsg2, sharedStart+sizeof(chaMsg2), sizeof(chaMsg2)));

    end_profiling();

    return 0;
}
