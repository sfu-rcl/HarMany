#include "board_support.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "HarMany.h"
#include "sha256.h"
#include "chacha.h"

#define CHA_MSG_LEN 128

static const BYTE shaInput1[] = {"abc"};
static const BYTE shaExpected1[SHA256_BLOCK_SIZE] = {
    0xBA, 0x78, 0x16, 0xBF, 0x8F, 0x01, 0xCF, 0xEA,
    0x41, 0x41, 0x40, 0xDE, 0x5D, 0xAE, 0x22, 0x23,
    0xB0, 0x03, 0x61, 0xA3, 0x96, 0x17, 0x7A, 0x9C,
    0xB4, 0x10, 0xFF, 0x61, 0xF2, 0x00, 0x15, 0xAD
};

static const BYTE shaInput2[] = {"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"};
static const BYTE shaExpected2[SHA256_BLOCK_SIZE] = {
    0x24, 0x8d, 0x6a, 0x61, 0xd2, 0x06, 0x38, 0xb8,
    0xe5, 0xc0, 0x26, 0x93, 0x0c, 0x3e, 0x60, 0x39,
    0xa3, 0x3c, 0xe4, 0x59, 0x64, 0xff, 0x21, 0x67,
    0xf6, 0xec, 0xed, 0xd4, 0x19, 0xdb, 0x06, 0xc1
};

static const BYTE shaInput3[] = {"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"};
static const BYTE shaExpected3[SHA256_BLOCK_SIZE] = {
    0xcf, 0x5b, 0x16, 0xa7, 0x78, 0xaf, 0x83, 0x80,
    0x03, 0x6c, 0xe5, 0x9e, 0x7b, 0x04, 0x92, 0x37,
    0x0b, 0x24, 0x9b, 0x11, 0xe8, 0xf0, 0x7a, 0x51,
    0xaf, 0xac, 0x45, 0x03, 0x7a, 0xfe, 0xe9, 0xd1
};

static const uint8_t chaExpected1[CHA_MSG_LEN] = {
    0x76, 0xb8, 0xe0, 0xad, 0xa0, 0xf1, 0x3d, 0x90,
    0x40, 0x5d, 0x6a, 0xe5, 0x53, 0x86, 0xbd, 0x28,
    0xbd, 0xd2, 0x19, 0xb8, 0xa0, 0x8d, 0xed, 0x1a,
    0xa8, 0x36, 0xef, 0xcc, 0x8b, 0x77, 0x0d, 0xc7,
    0xda, 0x41, 0x59, 0x7c, 0x51, 0x57, 0x48, 0x8d,
    0x77, 0x24, 0xe0, 0x3f, 0xb8, 0xd8, 0x4a, 0x37,
    0x6a, 0x43, 0xb8, 0xf4, 0x15, 0x18, 0xa1, 0x1c,
    0xc3, 0x87, 0xb6, 0x69, 0xb2, 0xee, 0x65, 0x86,
    0x9f, 0x07, 0xe7, 0xbe, 0x55, 0x51, 0x38, 0x7a,
    0x98, 0xba, 0x97, 0x7c, 0x73, 0x2d, 0x08, 0x0d,
    0xcb, 0x0f, 0x29, 0xa0, 0x48, 0xe3, 0x65, 0x69,
    0x12, 0xc6, 0x53, 0x3e, 0x32, 0xee, 0x7a, 0xed,
    0x29, 0xb7, 0x21, 0x76, 0x9c, 0xe6, 0x4e, 0x43,
    0xd5, 0x71, 0x33, 0xb0, 0x74, 0xd8, 0x39, 0xd5,
    0x31, 0xed, 0x1f, 0x28, 0x51, 0x0a, 0xfb, 0x45,
    0xac, 0xe1, 0x0a, 0x1f, 0x4b, 0x79, 0x4d, 0x6f
};

static BYTE shaCPU1[SHA256_BLOCK_SIZE];
static volatile BYTE shaHART[SHA256_BLOCK_SIZE];

static BYTE* sharedStart = (BYTE*) 0x8000E000;
volatile unsigned* shared = (unsigned*) 0x8000FFF0;

//It is good for this function to be slow as it holds up the accelerator
void hd(uint8_t id, uint16_t custom) {
    printf("Interrupt from accelerator %u with custom %hu\n", id, custom);
    int memRet = -1;
    if (id == 0) {
        if (custom == 2)
            memRet = memcmp(shaExpected2, sharedStart+SHA256_BLOCK_SIZE, SHA256_BLOCK_SIZE);
        else if (custom == 3)
            memRet = memcmp(shaExpected3, sharedStart+2*SHA256_BLOCK_SIZE, SHA256_BLOCK_SIZE);
    }
    else if (id == 1 && custom == 4) {
        memRet = memcmp(chaExpected1, sharedStart, CHA_MSG_LEN);
        *shared = 2;
    }

    printf("The comparison is %d\n", memRet);

    if (id == 0 && custom == 2) {
        printf("Calling the SHA accelerator with an interrupt from the handler...\n");
        options_t flags = {.interrupt = true, .return_if_busy = true, .return_if_full = true, .write_custom = true};
        uint32_t shaArgs[3] = {sizeof(shaInput3)-1, (uint32_t) shaInput3, (uint32_t) sharedStart+2*SHA256_BLOCK_SIZE};
        result_t res = call_accelerator(0, shaArgs, 3, 0, flags);
        if (res != SUCCESS)
            printf("Calling accelerator returned %d\n", res);
    }
}

int main(void) {
    //Platform Initialization
    platform_init ();

    //Records cycle and instruction counts
    start_profiling ();

    register_interrupt_handler(hd);

    result_t res;
    options_t flags;
    //Args: Length, Src*, Dst*
    uint32_t shaArgs[3];

    printf("Calling the SHA accelerator without an interrupt (to be checked later)...\n");
    flags = (options_t){.interrupt = false, .return_if_busy = false, .return_if_full = false, .write_custom = true};
    shaArgs[0] = sizeof(shaInput1)-1;
    shaArgs[1] = (uint32_t) shaInput1;
    shaArgs[2] = (uint32_t) sharedStart;
    res = call_accelerator(0, shaArgs, 1, 0, flags);
    if (res != SUCCESS) {
        printf("Calling accelerator returned %d\n", res);
        return -1;
    }

    printf("Calling the SHA accelerator with an interrupt...\n");
    flags = (options_t){.interrupt = true, .return_if_busy = false, .return_if_full = false, .write_custom = true};
    shaArgs[0] = sizeof(shaInput2)-1;
    shaArgs[1] = (uint32_t) shaInput2;
    shaArgs[2] = (uint32_t) sharedStart+SHA256_BLOCK_SIZE;
    res = call_accelerator(0, shaArgs, 2, 0, flags);
    if (res != SUCCESS) {
        printf("Calling accelerator returned %d\n", res);
        return -1;
    }

    printf("Running SHA in software...\n");
    SHA256_CTX sha_ctx;
    sha256_init(&sha_ctx);

    //In the meantime, compute the SHA using a library
    sha256_update(&sha_ctx, shaInput1, sizeof(shaInput1)-1);
    sha256_final(&sha_ctx, shaCPU1);
    printf("Comparing with initial hardware result...\n");
    printf("The comparison is %d\n", memcmp(shaCPU1, sharedStart, SHA256_BLOCK_SIZE));
    printf("Comparing with correct output...\n");
    printf("The comparison is %d\n", memcmp(shaCPU1, shaExpected1, SHA256_BLOCK_SIZE));

    printf("Telling core 1 to deliver us an interrupt...\n");
    *shared = 1;
    while (*shared != 2);

    printf("Sending 10 overlapping requests from both cores without interrupts to the first accelerator...\n");
    flags = (options_t){.interrupt = false, .return_if_busy = false, .return_if_full = false, .write_custom = false};
    shaArgs[0] = sizeof(shaInput3)-1;
    shaArgs[1] = (uint32_t) shaInput3;
    
    *shared = 3;
    for (int i = 0; i < 10; i += 2) {
        shaArgs[2] = (uint32_t) (sharedStart + i*SHA256_BLOCK_SIZE);
        res = call_accelerator(0, shaArgs, 0, 0, flags);
        if (res != SUCCESS)
            printf("Call accelerator surprisingly returned %d on iteration %d\n", res, i);
    }
    for (int i = 0; i < 10; i++) {
        printf("Comparison %d is %d\n", i, memcmp(sharedStart+i*SHA256_BLOCK_SIZE, shaExpected3, SHA256_BLOCK_SIZE));
    }

    end_profiling();

    printf("Handling control to core 1...\n");
    *shared = 4;

    return 0;
}
