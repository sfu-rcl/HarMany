# Example
This folder contains a modified runnable version of the demonstration outlined in doc/Presentation.pdf.

## Prerequisites
To run this demonstration, the following components are needed:
- A standard Linux environment (with make, unzip, and python3)
- The gcc bare-metal RISC-V toolchain (riscv32-unknown-elf)
  - Using [picolibc](https://github.com/picolibc/picolibc) as the standard C library
  - This can be readily created with [crosstool-NG](https://crosstool-ng.github.io/)
- [verilator](https://www.veripool.org/verilator/) for simulation
- [gtkwave](https://gtkwave.sourceforge.net/) for viewing simulation waveforms (optional)

This demonstration has been confirmed to run under:
- Ubuntu 22.04
- Verilator 5.024
- gcc 13.2.0
- binutils 2.42
- picolibc 1.8.3

## Running the demonstration
Before any make commands can be run, the riscv32-unknown-elf* toolchain and verilator must be added to the path.

There are a number of useful makefile targets:
1. `env`: Unzips the compressed environment containing the processor, hardware accelerators, and the simulation framework.
2. `sw`: Creates executables for both processors in the simulator from the source code in the sw folder.
3. `hw`: Creates an executable simulator binary of the hardware system.
4. `clean`: Removes all compiled code.

To run the simulation, simply run `make -j$(nproc)`, which will run all these targets and then execute the simulation.

## Debugging
The simulation binary provides several useful debug options:
- -l: logs the output of printf calls to .log files for each processor.
- -p: logs the program counter for each processor to a .pc file.
- -t: creates a waveform trace file that can be opened with gtkwave. This file can grow very large very quickly if the simulation runs into an infinite loop.

## Technical Details
The simulated processor in this example is a modified version of the one found [here](https://github.com/openhwgroup/cva5/commit/3239e20360993151f435fc2f5a567e09b3f185ad). These modifications primarily add support for multiple processors and fix a few important bugs that would otherwise prevent this example from working. Note that this processor does not support floating-point and atomic instructions.

If you wish to modify aspects of this example, there are several places you may wish to look at:
- Makefile: For adding new source files, changing compilation and linking flags, and modifying the layout of memory.
- env/tools/cva5.mak: To change the simulated delay properties of main memory.
- env/accelerators/\*/\*_wrapper.sv: To change the mailbox location in hardware.
- env/test_benches/verilator/cva5_sim.sv: To change the hardware setup and processor configurations.
- sw/HarManyCfg.h: For modifications to the HarMany library used in the software.
