/*
 * Copyright © 2022 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Chris Keilbart <ckeilbar@sfu.ca>
 */

module request_mux

    (
        input logic clk,
        input logic rst,

        memory_sub_unit_interface.responder low_prio,
        memory_sub_unit_interface.responder high_prio,
        memory_sub_unit_interface.controller l1
    );

    //L1 parameters mux between the two requests
    assign l1.new_request = high_prio.new_request | low_prio.new_request;
    always_comb begin
        if (high_prio.new_request) begin
            l1.addr = high_prio.addr;
            l1.re = high_prio.re;
            l1.we = high_prio.we;
            l1.be = high_prio.be;
            l1.data_in = high_prio.data_in;
        end
        else begin
            l1.addr = low_prio.addr;
            l1.re = low_prio.re;
            l1.we = low_prio.we;
            l1.be = low_prio.be;
            l1.data_in = low_prio.data_in;
        end
    end

    //Fifo keeps track of memory read request ordering
    fifo_interface #(.DATA_WIDTH(1)) mem_order_fifo();
    assign mem_order_fifo.data_in = high_prio.new_request;
    assign mem_order_fifo.push = l1.new_request & l1.re;
    assign mem_order_fifo.potential_push = l1.new_request & l1.re;
    assign mem_order_fifo.pop = l1.data_valid;
    //Currently only 2 requests can be outstanding at a time
    cva5_fifo #(.DATA_WIDTH(1), .FIFO_DEPTH(2)) mem_fifo (.fifo(mem_order_fifo), .*);

    //Low priority outputs
    assign low_prio.data_out = l1.data_out;
    assign low_prio.data_valid = l1.data_valid & ~mem_order_fifo.data_out;
    assign low_prio.ready = l1.ready & ~high_prio.new_request;
    
    //High priority outputs
    assign high_prio.data_out = l1.data_out;
    assign high_prio.data_valid = l1.data_valid & mem_order_fifo.data_out;
    assign high_prio.ready = l1.ready;

endmodule
