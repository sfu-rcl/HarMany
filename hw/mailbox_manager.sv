/*
 * Copyright © 2022 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Chris Keilbart <ckeilbar@sfu.ca>
 */

module mailbox_manager

    #(
        parameter logic[31:0] MAILBOX_ADDR = 32'h80000000,
        parameter logic[31:0] ARG_ADDR = MAILBOX_ADDR + 4,
        parameter NUM_ARGS = 4,
        parameter FIFO_DEPTH = 4
    )
    (
        input logic clk,
        input logic rst,

        input logic fifo_pop, //Once the first argument is popped, the rest must be popped every time they are valid
        output logic fifo_valid,
        output logic[31:0] fifo_data,

        memory_sub_unit_interface.controller i_mem
    );

    //Request fifo
    //Request arguments are pushed by the interface state machine and popped by the external controller state machine
    //Arguments are stored individually, with the first argument being a copy of the mailbox itself
    fifo_interface #(.DATA_WIDTH(32)) arg_fifo();
    assign arg_fifo.data_in = i_mem.data_out;
    assign arg_fifo.potential_push = arg_fifo.push;
    assign arg_fifo.pop = fifo_pop;
    assign fifo_valid = arg_fifo.valid;
    assign fifo_data = arg_fifo.data_out;
    cva5_fifo #(.DATA_WIDTH(32), .FIFO_DEPTH((NUM_ARGS+1)*FIFO_DEPTH)) argument_fifo (.fifo(arg_fifo), .*);

    //Interface state machine
    //The interface reads the arguments when indicated by the mailbox
    //The interface pushes the arguments to the fifo
    typedef enum {
        I_READY, //Mailbox = 00, written by us
        I_READ_ARGUMENTS, //Mailbox = 01 || 11, written by software
        I_WRITE_ACK, //Mailbox = 01 || 11, written by software
        I_FULL //Mailbox = 10, writen by us
    } i_state_t;
    i_state_t i_state = I_READY;
    i_state_t i_next_state = I_READY;

    always_ff @(posedge clk) begin
        if (rst)
            i_state <= I_READY;
        else
            i_state <= i_next_state;
    end
    logic[$clog2(NUM_ARGS):0] i_read_request_counter;
    logic[$clog2(NUM_ARGS)-1:0] i_read_response_counter;

    //Synchronous output forming logic
    always_ff @(posedge clk) begin
        if (rst) begin
            i_read_request_counter <= '0;
            i_read_response_counter <= '0;
        end
        else begin
            unique0 case (i_state)
                I_READY : begin
                    i_read_request_counter <= '0;
                    i_read_response_counter <= '0;
                end
                I_READ_ARGUMENTS : begin
                    if (i_mem.new_request)
                        i_read_request_counter <= i_read_request_counter + 1;
                    if (i_mem.data_valid)
                        i_read_response_counter <= i_read_response_counter + 1;
                end
            endcase
        end
    end

    //Combinational output forming logic
    assign i_mem.be = {3'b0, i_mem.we};
    assign i_mem.data_in = i_mem.ready & arg_fifo.full ? 32'h2 : 32'h0;
    always_comb begin
        unique case (i_state)
            I_READY : begin
                i_mem.addr = MAILBOX_ADDR;
                i_mem.re = 1;
                i_mem.we = 0;
                i_mem.new_request = i_mem.ready & ~(i_mem.data_valid & i_mem.data_out[0]) & ~rst;
                arg_fifo.push = i_mem.data_valid & i_mem.data_out[0]; //Push the mailbox if we're leaving
                i_next_state = i_mem.data_valid & i_mem.data_out[0] ? I_READ_ARGUMENTS : I_READY;
            end
            I_READ_ARGUMENTS : begin
                i_mem.addr = ARG_ADDR + 4*i_read_request_counter;
                i_mem.re = 1;
                i_mem.we = 0;
                i_mem.new_request = i_mem.ready & (i_read_request_counter != NUM_ARGS);
                arg_fifo.push = i_mem.data_valid;
                i_next_state = i_mem.data_valid & (i_read_response_counter == ($clog2(NUM_ARGS)'(NUM_ARGS-1))) ? I_WRITE_ACK : I_READ_ARGUMENTS;
            end
            I_WRITE_ACK : begin
                i_mem.addr = MAILBOX_ADDR;
                i_mem.re = 0;
                i_mem.we = 1;
                i_mem.new_request = i_mem.ready;
                arg_fifo.push = 0;
                i_next_state = i_mem.ready ? (arg_fifo.full ? I_FULL : I_READY) : I_WRITE_ACK;
            end
            I_FULL : begin
                i_mem.addr = MAILBOX_ADDR;
                i_mem.re = 0;
                i_mem.we = 1;
                i_mem.new_request = i_mem.ready & ~arg_fifo.full;
                arg_fifo.push = 0;
                i_next_state = i_mem.ready & ~arg_fifo.full ? I_READY : I_FULL;
            end
        endcase
    end
endmodule
