/*
 * Copyright © 2022 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 */

/// @file HarManyCfg.h
/// @author Chris Keilbart <ckeilbar@sfu.ca>
/// @version 1.0

#ifndef HARMANYCFG_H
#define HARMANYCFG_H

//Common options to all configurations
#define NUM_ACCELERATORS 2 ///< Compile time definition of the number of hardware accelerators
#define MAILBOX_ADDRESSES {0x8000F000, 0x8000F100} ///< Compile time mailbox addresses for each accelerator. Each mailbox takes 4 bytes.
#define ARG_BASE_ADDRESSES {0x8000F004, 0x8000F104} ///< Compile time first-argument addresses for each accelerator
#define ARG_COUNTS {3, 8} ///< The expected number of 32-bit arguments for each accelerator

#define CHECK_INPUT 1 ///< Configure whether the library functions perform runtime input checks

#define INTERRUPT 1 ///< Configure whether the library interrupt functions are available

#define NUM_CORES 2 ///< Configure the amount of processors in the system

//Thread safety support is optional but highly recomended
#if NUM_CORES > 1 || INTERRUPT != 0
    #define THREAD_SAFE 1 ///< Determines whether operations use "mutexes"
    #if THREAD_SAFE != 0
        #define USE_ATOMIC 0 ///< If ::THREAD_SAFE is enabled, configure whether the library will issue atomic instructions for synchronizsation
        #if NUM_CORES > 1
            /// @brief Location of mutexes in shared memory
            #define MUTEX_BASE_ADDRESS 0x8000FF00 ///< If ::USE_ATOMIC is set, this uses ::NUM_CORES*4 bytes. If not, 2*::NUM_CORES*4 bytes are used.
        #endif
    #endif
#endif

/// \example example_config.h
/// This example depicts a sample compile-time library configuration.

#endif
