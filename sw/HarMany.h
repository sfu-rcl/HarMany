/*
 * Copyright © 2022 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 */

/// @file HarMany.h
/// @author Chris Keilbart <ckeilbar@sfu.ca>
/// @version 1.0


#ifndef HARMANY_H
#define HARMANY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "HarManyCfg.h"

/// @brief Return value enum, >= 0 for success and < 0 for failure
typedef enum {
    SUCCESS = 0, ///< Operation completed successfully
    BAD_ACCELERATOR_INDEX = -100, ///< Accelerator index argument is out of range
    BAD_CPU_INDEX, ///< CPU argument is out of range
    INTERFACE_BUSY, ///< The hardware accelerator interface is already in use
    INTERFACE_FULL, ///< The hardware accelerator request FIFO is full
    LOCK_ALREADY_HELD ///< The hardware accelerator interface is currently in use by this CPU
} result_t;

/// @brief Options for interfacing with an accelerator
typedef struct {
    bool interrupt:1; ///< Whether an interrupt should be delivered when the accelerator finishes computing this request
    bool return_if_busy:1; ///< Do not block if the interface is busy with another request, should be set if calling from an interrupt handler
    bool return_if_full:1; ///< Do not block if the interface is full and cannot handle any requests, must be set if calling from an interrupt handler
    bool write_custom:1; ///< Write the custom message to the mailbox
} options_t;

/// @brief Reads the upper 16 bits of the specified mailbox
/// @param accelerator The accelerator mailbox to query
/// @param dst Where the value is stored
/// @return Either ::SUCCESS or ::BAD_ACCELERATOR_INDEX
result_t read_custom(uint8_t accelerator, uint16_t* dst);

/// @brief Writes the upper 16 bits of the specified mailbox
/// @param accelerator The accelerator mailbox to write
/// @param msg The value to write
/// @return Either ::SUCCESS or ::BAD_ACCELERATOR_INDEX
result_t write_custom(uint8_t accelerator, uint16_t msg);

/// @brief Waits until the accelerator is ready for input
///
/// Not safe to call from an interrupt handler.
/// @param accelerator The accelerator to wait for
/// @return Either ::SUCCESS or ::BAD_ACCELERATOR_INDEX
result_t wait_ready(uint8_t accelerator);

/// @brief Enqueues a request to a hardware accelerator
/// @param accelerator The accelerator to call
/// @param args A pointer to an array of arguments that will be passed to the hardware accelerator
/// @param custom The custom 16 bits that are optionally written in the mailbox
/// @param cpu The CPU to optionally interrupt
/// @param flags Modifications to the default behaviour
/// @return ::SUCCESS, ::BAD_ACCELERATOR_INDEX, ::BAD_CPU_INDEX, ::LOCK_ALREADY_HELD, ::INTERFACE_BUSY, ::INTERFACE_FULL
result_t call_accelerator(uint8_t accelerator, uint32_t const* args, uint16_t custom, uint8_t cpu, options_t flags);

#ifdef INTERRUPT
/// @brief An initial function to enable interrupts and set the interrupt handler that will be called when a hardware accelerator has finished
/// @param handler The function to be called when this core is interrupted that takes the accelerator ID as the first argument and the custom 16 bits as the second argument
void register_interrupt_handler(void (*handler)(uint8_t, uint16_t));

/// @brief Changes the interrupt handler function
/// @param handler The function to be called when this core is interrupted that takes the accelerator ID as the first argument and the custom 16 bits as the second argument
void change_interrupt_handler(void (*handler)(uint8_t, uint16_t));
#endif

/// \example example_usage.c
/// This example demonstrates use of the HarMany software library.


#ifdef __cplusplus
}
#endif
#endif