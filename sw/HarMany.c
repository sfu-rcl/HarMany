/*
 * Copyright © 2022 Chris Keilbart, Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 */

/// @file HarMany.c
/// @author Chris Keilbart <ckeilbar@sfu.ca>
/// @version 1.0

#include <stdatomic.h>
#include <assert.h>
#include "HarManyCfg.h"
#include "HarMany.h"

//16b custom, 6b response CPU, 1b reserved, 1b response valid, 6b target interrupt CPU, 2b state
#define READY 0 ///< Accelerator interface is currently free
#define FULL 2 ///< Accelerator interface FIFO is full
#define PENDING 1 ///< Written when a new request is issued to an accelerator
#define PENDING_INTERRUPT 3 ///< Written when a new request is issued to an accelerator and a response interrupt to the target processor is desired
#define STATE_MASK 3 ///< Bitmask for the accelerator state

//Constant variables
static const uint32_t arg_base_int[] = ARG_BASE_ADDRESSES; ///< Raw array of argument base addresses
static volatile uint32_t *const *const arg_bases = (volatile uint32_t *const *const) arg_base_int; ///< Pointer array of argument base addresses
static const uint8_t arg_counts[] = ARG_COUNTS; ///< Expected argument counts for each hardware accelerator
static const uint32_t mailbox_int[] = MAILBOX_ADDRESSES; ///< Raw array of hardware accelerator mailbox addresses
static volatile uint32_t *const *const mailboxes = (volatile uint32_t *const *const) mailbox_int; ///< Pointer array of accelerator mailbox addresses

//Static compile-time assertions to check the config
static_assert(NUM_CORES > 0 && NUM_CORES <= 64, "Bad core count"); //6b for the interrupt CPU
static_assert(NUM_ACCELERATORS > 0 && NUM_ACCELERATORS <= 256, "Bad accelerator count"); //8b function arguments
static_assert(NUM_ACCELERATORS == sizeof(mailbox_int)/sizeof(uint32_t), "Bad mailbox count");
static_assert(NUM_ACCELERATORS == sizeof(arg_base_int)/sizeof(uint32_t), "Bad argument base address count");
static_assert(NUM_ACCELERATORS == sizeof(arg_counts)/sizeof(uint8_t), "Bad argument length count");
#if THREAD_SAFE != 0 && NUM_CORES > 1 && !defined(MUTEX_BASE_ADDRESS)
#error "No mutex address defined"
#endif


//Convenience macros and functions
#if INTERRUPT != 0 || THREAD_SAFE != 0
/// @brief Gets the ID of the processor that executes the function from the CSR
/// @return the processor ID
static inline uint32_t get_TID(void) {
    uint32_t tid;
    asm (
        "csrr %0, mhartid;"
        : "=r" (tid)
    );
    return tid;
}
#endif

#if THREAD_SAFE != 0
#if NUM_CORES == 1
/// If val == 0, the lock is open.
/// If val > 0, the lock is held by processor (val + 1).
static atomic_uint mutexes[1]; ///< Synchronization "mutex" for software access to hardware accelerators
#else
/// If val == 0, the lock is open.
/// If val > 0, the lock is held by processor (val + 1).
static atomic_uint* mutexes = (atomic_uint*) (MUTEX_BASE_ADDRESS); ///< Synchronization "mutexes" for software access to hardware accelerators
#endif

/// Unlocks the specified "mutex" by setting it back to 0
#define unlock(accelerator) do {atomic_store_explicit(&mutexes[(accelerator)], 0, memory_order_release);} while (0)

#endif

//Begin implementation

//Interrupt support
#if INTERRUPT != 0
static void (*registered_handlers[NUM_CORES])(uint8_t, uint16_t); ///< Manually registered interrupt handlers

/// @brief Calls the specified interrupt handler for each hardware accelerator with a pending interrupt
///
/// This function is invoked automatically by the hardware when an interrupt from a hardware accelerator occurs.
/// All pending interrupts are handled before the handler returns.
/// 
/// This function is annotated with the machine interrupt attribute, meaning it automatically pushes and pops all registers to the stack and uses MRET to return.
/// Additionally, this function cannot be interrupted as interrupts are automatically disabled until the MRET is executed.
static void irq_handler(void) __attribute__ ((interrupt ("machine"))); //Required for safety (pushes/pops all registers to stack, uses MRET)
static void irq_handler(void) {
    uint32_t tid = get_TID();
    uint8_t expected = (tid << 2) | 3;
    uint32_t mailbox_value;
    const uint32_t mip_pending = 1 << 11; //MEIP is index 11
    uint32_t mip;
    uint16_t custom;

    //This function is critically important and must satisfy the following points:
    //A. Every interrupt causes the registered handler to be invoked
    //B. The registered handler cannot be spurriously invoked

    //There are two problems for the implementation that need to be considered to guarantee the above points
    //1: An interrupt caused this function to be executed but no pending interrupt was seen in a mailbox
    //This is possible because the interrupt is delivered immediately by a hardware accelerator
    //However, the data in the mailbox involves the memory subsystem, which has unpredictable latency
    //The solution to this problem is to wait in this function until a mailbox interrupt is visible
    //But this leads to another problem
    //2: Clearing the interrupt by writing to the mailbox has an unknown latency
    //By the time the MIP register is reloaded, the interrupt signal may still be held because *mail_ptr = 0 may not be visible
    //This would only be possible if the registered handler was very short
    //This still needs to be handled because a spurious call to this function would hang because of the solution to 1
    //The solution is therefore to only exit this function if there are no more pending interrupts
    do {
        //Iterate over accelerators to find the cause of the interrupt
        //All accelerators are checked, and the handler will be called for each eligible accelerator
        for (uint8_t i = 0; i < NUM_ACCELERATORS; i++) {
            mailbox_value = *mailboxes[i];
            if ((mailbox_value << 16) >> 24 == expected) { //Extract bits 15-8
                custom = mailbox_value >> 16; //Mailbox must be read before the write (as it may change after the accelerator is resumed)
                *(((uint8_t*) mailboxes[i]) + 1) = 0; //Causes the accelerator to stop issuing the interrupt and resume
                registered_handlers[tid](i, custom);
            }
        }
        //We need to clear the interrupt pending bit to prevent the IRQ from looping
        //This write to MIP does not actually do anything, as MEIP is read only in the MIP register
        //However, it forces a reload of the MIP register from the external bits, which may clear the bit
        asm volatile ( //Volatile is required because otherwise it thinks mip always == mip_pending
            "csrs mip, %1\n" //Write MIP to force a reload
            "csrr %0, mip" //Read the new value
            : "=r" (mip)
            : "r" (mip_pending)
        );
    } while (mip == mip_pending); //Repeat this function until there are no more interrupts

    //If interrupts become pending during the return this function is automatically called again
    return;
}

void register_interrupt_handler(void (*handler)(uint8_t, uint16_t)) {
    registered_handlers[get_TID()] = handler;

    uint32_t irq = (uint32_t) &irq_handler;
    uint32_t mie = (1 << 11);
    asm (
        "csrw mtvec, %0;\n" //Set the IRQ vector to our main handler
        "csrs mie, %1;\n" //Enable external machine interrupts
        "csrsi mstatus, 8" //Enable machine interrupts (bit 3)
        :
        : "r" (irq), "r" (mie)
    );
    return;
}

void change_interrupt_handler(void (*handler)(uint8_t, uint16_t)) {
    registered_handlers[get_TID()] = handler;
    return;
}
#endif

#if THREAD_SAFE != 0
#if USE_ATOMIC == 0
/// @brief States used in Szymański's mutual exclusion algorithm
///
/// The relative numerical values of the states are significant.
enum flag_state {
    NONCRITICAL, ///< Processor is not in and doesn't want to be in the critical section
    INTENTION, ///< Processor wishes to enter critical section
    DOOR, ///< Waiting for other processors that signified intention to enter
    WAITING_ROOM, ///< In the final waiting room before the critical section
    CRITICAL_SECTION ///< Processor is in the critical section
};
static atomic_int* flags = (atomic_int*) (MUTEX_BASE_ADDRESS+4*NUM_CORES);

#endif
/// @brief Attempts to lock the specified accelerator mutex
///
/// Depending on USE_ATOMIC, the implementation uses either atomic CAS or [Szymański's mutual exclusion algorithm](https://en.wikipedia.org/wiki/Szyma%C5%84ski%27s_algorithm).
/// @param accelerator The accelerator to try and acquire the mutex for
/// @return -1 if the lock was already held by this processor, 0 if the lock is held by someone else, and 1 if the lock was acquired
static int try_lock(uint8_t accelerator) {
    int ret = 0;
    int tid = get_TID();
    uint32_t tid_write = tid + 1; //The value to write in the mutex cannot be 0 because that is the unlocked value
#if USE_ATOMIC != 0
    uint32_t expected = 0;
    bool res = atomic_compare_exchange_strong_explicit(&mutexes[accelerator], &expected, tid_write, memory_order_acq_rel, memory_order_relaxed);
    if (res)
        ret = 1;
    else if (expected == tid_write)
        ret = -1;
#else
    if (flags[tid] != NONCRITICAL) //The processor was already in or trying to enter the critical section
        return -1;
    
    flags[tid] = INTENTION;
    int i = 0;
    while (i < NUM_CORES) //Wait for the door to be open
        i = flags[i] > DOOR ? 0 : i+1;
    flags[tid] = WAITING_ROOM;

    for (i = 0; i < NUM_CORES; i++) {
        if (flags[i] == INTENTION) { //Another processor wants to enter, wait for them
            flags[tid] = DOOR;
            i = NUM_CORES;
            do { //Wait for someone to close the door
                i = i == NUM_CORES ? 0 : i+1;
            } while (flags[i] != CRITICAL_SECTION);
            break;
        }
    }
    flags[tid] = CRITICAL_SECTION; //Close the door
    i = 0;
    while (i < tid) //Wait for lower processors to finish
        i = flags[i] > INTENTION ? 0 : i + 1;
    //Critical section
    
    uint32_t val = mutexes[accelerator];
    if (val == 0) {
        mutexes[accelerator] = tid_write;
        ret = 1;
    }
    else if (val == tid_write)
        ret = -1;

    //Exit critical section
    i = tid_write;
    while (i < NUM_CORES) //Wait for everyone in the waiting room to recognize the door is closed
        i = (flags[i] == DOOR || flags[i] == WAITING_ROOM) ? tid_write : (uint32_t) i+1;
    flags[tid] = NONCRITICAL; //Leave
#endif
    return ret;
}
#endif


result_t read_custom(uint8_t accelerator, uint16_t* dst) {
#if CHECK_INPUT != 0
    if (accelerator >= NUM_ACCELERATORS)
        return BAD_ACCELERATOR_INDEX;
#endif

    *dst = *(((uint16_t*) mailboxes[accelerator]) + 1);
    return SUCCESS;
}

result_t write_custom(uint8_t accelerator, uint16_t msg) {
#if CHECK_INPUT != 0
    if (accelerator >= NUM_ACCELERATORS)
        return BAD_ACCELERATOR_INDEX;
#endif

    *(((uint16_t*) mailboxes[accelerator]) + 1) = msg;
    return SUCCESS;
}

result_t wait_ready(uint8_t accelerator) {
#if CHECK_INPUT != 0
    if (accelerator >= NUM_ACCELERATORS)
        return BAD_ACCELERATOR_INDEX;
#endif

    uint8_t status;
    do
        status = *((uint8_t*) mailboxes[accelerator]);
    while ((status & STATE_MASK) != READY);

    return SUCCESS;
}


result_t call_accelerator(uint8_t accelerator, uint32_t const* args, uint16_t custom, uint8_t cpu, options_t flags) {
#if CHECK_INPUT != 0
    if (accelerator >= NUM_ACCELERATORS)
        return BAD_ACCELERATOR_INDEX;
    if (cpu >= 64)
        return BAD_CPU_INDEX;
#endif

    uint32_t mail;

#if THREAD_SAFE != 0
    while (true) {
        //We first try to grab the lock - we must do this BEFORE reading the mailbox to prevent deadlock if this CPU already has the lock
        int lock_ret = try_lock(accelerator);
        if (lock_ret == -1) //We already held this lock - return to prevent deadlock
            return LOCK_ALREADY_HELD;
        else if (lock_ret == 1) { //We have the lock - double check the state
            mail = *mailboxes[accelerator] & STATE_MASK;
            if (mail == READY) //Lock is held and accelerator is ready - enter critical section
                break;
            unlock(accelerator); //We have the lock but cannot proceed because the accelerator isn't ready
        }
#endif
        //Now wait until the accelerator is ready - the lock IS NOT HELD here
        do {
            mail = *mailboxes[accelerator] & STATE_MASK;
            if (flags.return_if_busy && (mail & PENDING))
                return INTERFACE_BUSY;
            if (flags.return_if_full && (mail == FULL))
                return INTERFACE_FULL;
        } while (mail != READY);
#if THREAD_SAFE != 0
    }
#endif

    //Start critical section - the lock is held at this point and the accelerator is ready

    //Write arguments to registers
    uint8_t i = 0;
    while (i < arg_counts[accelerator]) {
        arg_bases[accelerator][i] = args[i];
        i++;
    }
    
    if (flags.write_custom)
        write_custom(accelerator, custom);

    //Ensure arguments are written before mailbox
    atomic_thread_fence(memory_order_seq_cst);

    uint8_t write_mail = cpu << 2; //6b target CPU
    write_mail |= (flags.interrupt) << 1; //1b interrupt 
    write_mail |= 1; //1b pending
    *((uint8_t*) mailboxes[accelerator]) = write_mail;

    //End critical section
#if THREAD_SAFE != 0
    unlock(accelerator);
#endif

    return SUCCESS;
}
